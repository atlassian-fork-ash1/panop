/* eslint-disable import/no-extraneous-dependencies */

const webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');

module.exports = {
  entry: ['./src/index.js'],
  output: {
    path: __dirname,
    filename: 'dist/bundle.js',
    libraryTarget: 'umd',
    library: 'panop',
  },
  module: {
    loaders: [
      {
        test: /src\/.+\.js?$/,
        loader: 'babel-loader',
      },
    ],
  },
  target: 'node',
  externals: [nodeExternals()],
  node: {
    fs: 'empty',
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production'),
      },
    }),
    new webpack.optimize.DedupePlugin(),
  ],
};
