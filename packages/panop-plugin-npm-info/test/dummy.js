import chai from 'chai'; // eslint-disable-line import/no-extraneous-dependencies
import generateRegistryUrl from '../src/generateRegistryUrl';

chai.should();

describe('panop-plugin-npm-info', () => {
  describe('generateRegistryUrl()', () => {
    it('should escape scoped packages properly', () => {
      generateRegistryUrl('@atlaskit/badge').should.equal('https://registry.npmjs.org/@atlaskit%2Fbadge');
    });
    it('should escape unscoped packages properly', () => {
      generateRegistryUrl('ak-badge').should.equal('https://registry.npmjs.org/ak-badge');
    });
  });
});
