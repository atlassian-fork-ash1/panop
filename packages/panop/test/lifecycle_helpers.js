import chai from 'chai'; // eslint-disable-line import/no-extraneous-dependencies
import chaiAsPromised from 'chai-as-promised'; // eslint-disable-line
import {
  groupTagsForComponents,
  parseComponentTag,
} from '../src/lifecycle_helpers';

chai.should();

describe('lifecycle_helpers', () => {
  describe('groupTagsForComponents()', () => {
    it('should handle items with prefixes and npm scope correctly', () => {
      const components = [
        { dirName: 'ak-button', pkgName: 'ak-button' },
        { dirName: 'ak-editor', pkgName: 'ak-editor' },
        { dirName: 'ak-editor-button', pkgName: 'ak-editor-button' },
        { dirName: 'badge', pkgName: '@atlaskit/badge' },
      ];
      const tags = [
        'ak-button@0.0.1',
        'ak-editor@1.0.0',
        'ak-editor@1.0.1',
        'ak-editor-button@2.0.0',
        '@atlaskit/badge@1.2.3',
      ];
      groupTagsForComponents(components, tags).should.deep.equal({
        components: [
          { dirName: 'ak-button', pkgName: 'ak-button', tags: ['ak-button@0.0.1'] },
          { dirName: 'ak-editor', pkgName: 'ak-editor', tags: ['ak-editor@1.0.0', 'ak-editor@1.0.1'] },
          { dirName: 'ak-editor-button', pkgName: 'ak-editor-button', tags: ['ak-editor-button@2.0.0'] },
          { dirName: 'badge', pkgName: '@atlaskit/badge', tags: ['@atlaskit/badge@1.2.3'] },
        ],
        totalVersions: 5,
      });
    });
  });

  describe('parseComponentTag()', () => {
    it('should handle unscoped package names', () => {
      parseComponentTag('ak-badge@1.2.3').should.deep.equal({
        scopedPackageName: 'ak-badge',
        packageName: 'ak-badge',
        packageVersion: '1.2.3',
      });
    });
    it('should handle scoped package names', () => {
      parseComponentTag('@atlaskit/badge@3.2.1').should.deep.equal({
        scopedPackageName: '@atlaskit/badge',
        packageName: 'badge',
        packageVersion: '3.2.1',
      });
    });
  });
});
