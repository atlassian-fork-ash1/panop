// @flow weak

export function groupTagsForComponents(components, tags) {
  let totalVersions = 0;

  const newComponents = components.map((component) => {
    const { dirName, pkgName } = component;
    const componentTags = tags.filter(tag => tag.indexOf(`${pkgName}@`) === 0);

    totalVersions += componentTags.length;
    return {
      dirName,
      pkgName,
      tags: componentTags,
    };
  });

  return {
    components: newComponents,
    totalVersions,
  };
}

export function calculateLatestPublishedVersion(sortedVersions) {
  return sortedVersions.find(ver => ver.npmInfo && ver.npmInfo.isPublished);
}

export function mergeDataForTag(mergeData, packageName, packageVersion) {
  if (!mergeData) return null;

  const mergeComponent = mergeData.components.find(c => c.name === packageName);
  if (mergeComponent) {
    return mergeComponent.versions.find(v => v.version === packageVersion);
  }
  return null;
}

export function parseComponentTag(tag) {
  const parts = tag.split('@');
  if (tag.indexOf('@') === 0) {
    const [, unprefixedPkgScopeAndName, packageVersion] = parts;
    return {
      scopedPackageName: `@${unprefixedPkgScopeAndName}`,
      packageName: unprefixedPkgScopeAndName.split('/')[1],
      packageVersion,
    };
  }

  const [packageName, packageVersion] = parts;
  return {
    scopedPackageName: packageName,
    packageName,
    packageVersion,
  };
}
