// @flow weak

module.exports = class Bitbucket {
  account: string;
  repo: string;
  reqThrottle: Object;

  constructor(options) {
    this.account = options.account;
    this.repo = options.repo;
    this.reqThrottle = options.reqThrottle;
  }

  bitbucketApi(opts) {
    return this.reqThrottle.add({
      url: `https://api.bitbucket.org/${opts.endpoint}`,
      auth: {
        user: process.env.BITBUCKET_USER,
        pass: process.env.BITBUCKET_PASS,
      },
      json: true,
      ttl: 60 * 60 * 24,
      gzip: true,
    }).then(({ body }) => {
      let result = opts.subkey ? body[opts.subkey] : body;
      if (opts.map) {
        result = result.map(opts.map);
      }
      return result;
    });
  }

  bitbucketLs(path, opts) {
    const options = Object.assign({
      tag: 'master',
    }, opts);

    const { account, repo } = this;

    options.endpoint = `/1.0/repositories/${account}/${repo}/src/${options.tag}/${path}`;

    return this.bitbucketApi(options);
  }

  getTags() {
    return this.bitbucketApi({
      endpoint: `/1.0/repositories/${this.account}/${this.repo}/branches-tags`,
      subkey: 'tags',
      map: item => item.name,
    });
  }

  getFileContents(opts) {
    const options = Object.assign({
      tag: 'master',
      path: 'package.json',
    }, opts);

    const { account, repo } = this;
    const { tag, path } = options;
    return this.bitbucketApi({
      endpoint: `/1.0/repositories/${account}/${repo}/raw/${tag}/${path}`,
    });
  }

  getAllFileContents(opts) {
    return this.bitbucketLs(opts.path, {
      tag: opts.tag,
      subkey: 'files',
    }).then((allFiles) => {
      if (!allFiles) {
        return [];
      }

      const fileContentPromises = [];
      const filePattern = new RegExp(`.${opts.extension}$`);
      allFiles.forEach((fileInfo) => {
        if (fileInfo.path.match(filePattern)) {
          fileContentPromises.push(
            this.getFileContents({
              tag: opts.tag,
              path: fileInfo.path,
            })
          );
        }
      });

      return Promise.all(fileContentPromises);
    });
  }

};
