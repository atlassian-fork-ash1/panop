#! /usr/bin/env node
/* eslint-disable no-console */

import 'babel-polyfill';
import main from './index';
import Bitbucket from './bitbucket';
import RequestThrottle from './util/request-throttle';

const argv = require('yargs').argv;

const { requestsPerSecond = 10, ...otherArgs } = argv;
const repoParts = argv.repo.split('/');
const reqThrottle = new RequestThrottle(requestsPerSecond);

const exitWithError = (error) => {
  console.log(error);
  process.exit(1);
};

try {
  main({
    gitProvider: new Bitbucket({
      account: repoParts[0],
      repo: repoParts[1],
      reqThrottle,
    }),
    reqThrottle,
    ...otherArgs,
  }).catch(exitWithError);
} catch (e) {
  exitWithError(e);
}
