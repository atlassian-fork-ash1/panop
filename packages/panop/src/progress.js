// @flow weak

import ProgressBar from 'progress';

let bar;

export function start(total) {
  bar = new ProgressBar(':bar', {
    total,
    width: 80,
  });
}

export function promiseWithProgress(prom) {
  return prom.then((result) => {
    bar.tick();
    return result;
  });
}

export function tick(count = 1) {
  for (let i = 0; i < count; i += 1) {
    bar.tick();
  }
}
