// @flow weak

import YAML from 'json2yaml';
import { writeFiles } from './utils';
import Lifecycle from './lifecycle';

const log = console.log; // eslint-disable-line no-console

async function main(options) {
  if (!process.env.BITBUCKET_USER || !process.env.BITBUCKET_PASS) {
    throw new Error('Please set BITBUCKET_USER and BITBUCKET_PASS environment variables');
  }

  if (!options.gitProvider.account || !options.gitProvider.repo) {
    throw new Error('Please provide a --repo value in the format "atlassian/atlaskit"');
  }

  if (!options.yml && !options.json) {
    throw new Error('Please provide a --yml or --json flag (or both)');
  }

  console.time('panop execution'); // eslint-disable-line no-console

  // NOTE: Always need pkg-json here, and first
  const plugins = ['pkg-json-info'].concat(options.plugins.split(',').filter(p => !!p)).map((pluginShortName) => {
    const PluginClass = require(`panop-plugin-${pluginShortName}`); // eslint-disable-line
    return new PluginClass();
  });

  const lc = new Lifecycle({
    plugins,
    git: options.gitProvider,
    mergeData: options.mergeData,
  });

  log('Getting list of components from Bitbucket repo...');
  const { pkgScope, maxComponents } = options;
  const components = await lc.getComponents({ pkgScope, maxComponents });

  log('Getting version list for each component...');
  const componentsWithTags = await lc.addTagsToComponents(components);

  log(`Processing ${componentsWithTags.totalVersions} component versions (${options.reqThrottle.requestsPerSecond} API requests per second)...`);
  const processedComponents = await lc.processComponents(componentsWithTags);

  const outputFiles = [];

  if (options.yml) {
    outputFiles.push({
      name: options.yml,
      content: YAML.stringify(processedComponents),
    });
  }

  if (options.json) {
    outputFiles.push({
      name: options.json,
      content: {
        generated: Date.now(),
        repository: `${options.gitProvider.account}/${options.gitProvider.repo}`,
        components: processedComponents,
      },
    });
  }

  writeFiles(outputFiles);
  log(`Success! ${outputFiles.length} file(s) written.`);

  console.timeEnd('panop execution'); // eslint-disable-line no-console
}

export default main;
