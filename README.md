# panop

![npm](https://img.shields.io/npm/v/panop.svg)
![build](https://bitbucket-badges.atlassian.io/badge/atlassian/panop.svg)
![commitizen](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)

panop processes the packages in your [Lerna](https://lernajs.io/)-style monorepo through a set of filters, to produce a single consolidated monorepo information file describing key aspects of each package.

**Note:** This is a monorepo for the main `panop` tool plus the related plugins. Please see the relevant `README.md` for details of a specific package in this monorepo.
